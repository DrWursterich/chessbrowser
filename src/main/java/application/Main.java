package application;

import chess.Game;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.stage.Stage;
import resource.ResourceManager;

public class Main extends Application {
	@FXML
	private Game game;

	@Override
	public void start(final Stage primaryStage) throws Exception {
		primaryStage.setScene(new Scene(
				ResourceManager.loadNewResource("application.Main", this),
				1400,
				800));
		this.game.setup();
		primaryStage.show();
	}

	public static void main(final String[] args) throws Exception {
		Application.launch(args);
	}
}
