package chess;

import java.util.concurrent.atomic.AtomicBoolean;

import javafx.scene.layout.StackPane;

import chess.move.Move;
import chess.piece.Piece;
import chess.piece.movement.Movement;

public class Field extends StackPane {
	private static final String[] ROW_IDENTIFIERS = {
			"8", "7", "6", "5", "4", "3", "2", "1"
	};
	private static final String[] COLUMN_IDENTIFIERS = {
			"A", "B", "C", "D", "E", "F", "G", "H"
	};
	private final Board board;
	private final int column;
	private final int row;
	private Piece piece;

	public Field(
			final Board board,
			final int column,
			final int row,
			final Piece piece) {
		this.board = board;
		this.column = column;
		this.row = row;
		this.piece = piece;
		this.prefWidthProperty().bind(board.sizeProperty().divide(Board.SIZE));
		this.prefHeightProperty().bind(board.sizeProperty().divide(Board.SIZE));
		this.setOnMouseClicked(e -> {
			this.board.clickedOnField(this);
		});
	}

	public Field(final Board board, final int column, final int row) {
		this(board, column, row, null);
	}

	public int getColumn() {
		return this.column;
	}

	public int getRow() {
		return this.row;
	}

	public Piece getPiece() {
		return this.piece;
	}

	public void setPiece(final Piece piece) {
		this.piece = piece;
		this.getChildren().clear();
		if (this.piece != null) {
			this.piece.setField(this);
			this.getChildren().add(this.piece);
		}
	}

	public void addPiece(final Piece piece) {
		this.board.addPieceAt(piece, this.getColumn(), this.getRow());
	}

	public Piece capturePiece() {
		final Piece captured = this.piece;
		if (captured != null) {
			this.board.removePiece(captured);
		}
		return captured;
	}

	public Board getBoard() {
		return this.board;
	}

	public Field getRelativeField(final int column, final int row) {
		final int x = this.column + column;
		final int y = this.row + row;
		return this.board.fieldExists(x, y)
				? this.board.getField(x, y)
				: null;
	}

	public void setHighlited(final boolean highlighted) {
		this.getStyleClass().remove("highlighted");
		if (highlighted) {
			this.getStyleClass().add("highlighted");
		}
	}

	public boolean getHighlited() {
		return this.getStyleClass().contains("highlighted");
	}

	public String getRowIdentifier() {
		return Field.ROW_IDENTIFIERS[this.row];
	}

	public String getColumnIdentifier() {
		return Field.COLUMN_IDENTIFIERS[this.column];
	}

	public boolean isAttackedBy(final Color color, final Piece pieceToIgnore) {
		final AtomicBoolean result = new AtomicBoolean(false);
		for (final Movement movement : Movement.values()) {
			movement.forEachAttackingPiece(
					this,
					pieceToIgnore,
					e -> {
						result.set(result.get() || (e.getColor() == color));
						return result.get();
					});
		}
		return result.get();
	}

	public boolean isAttackedByOtherThan(
			final Color color,
			final Piece pieceToIgnore) {
		final AtomicBoolean result = new AtomicBoolean(false);
		for (final Movement movement : Movement.values()) {
			movement.forEachAttackingPiece(
					this,
					pieceToIgnore,
					e -> {
						result.set(result.get() || (e.getColor() != color));
						return result.get();
					});
		}
		return result.get();
	}

	public boolean wouldBeAttackedByOtherThan(
			final Move move,
			final Color color) {
		move.execute();
		final AtomicBoolean result = new AtomicBoolean(false);
		for (final Movement movement : Movement.values()) {
			movement.forEachAttackingPiece(
					this,
					null,
					e -> {
						result.set(result.get() || (e.getColor() != color));
						return result.get();
					});
		}
		move.undo();
		return result.get();
	}

	@Override
	public String toString() {
		return this.getColumnIdentifier() + this.getRowIdentifier();
	}
}

