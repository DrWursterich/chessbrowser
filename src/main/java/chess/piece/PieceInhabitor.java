package chess.piece;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import chess.Color;
import chess.Field;
import chess.piece.King;
import chess.piece.Piece;

public class PieceInhabitor {

	private final Map<Color, PieceMap> pieceMap;

	private final class PieceMap
			extends HashMap<Class<? extends Piece>, List<Piece>> {
	}

	public PieceInhabitor() {
		this.pieceMap = new HashMap<>();
	}

	public void addPiece(final Piece piece) {
		PieceMap colorMap = this.pieceMap.get(piece.getColor());
		if (colorMap == null) {
			colorMap = new PieceMap();
			this.pieceMap.put(piece.getColor(), colorMap);
		}
		List<Piece> pieces = colorMap.get(piece.getClass());
		if (pieces == null) {
			pieces = new ArrayList<>(8);
			colorMap.put(piece.getClass(), pieces);
		}
		pieces.add(piece);
	}

	public void removePiece(final Piece piece) {
		PieceMap colorMap = this.pieceMap.get(piece.getColor());
		if (colorMap == null) {
			return;
		}
		List<Piece> pieces = colorMap.get(piece.getClass());
		if (pieces == null) {
			return;
		}
		pieces.remove(piece);
		if (pieces.isEmpty()) {
			colorMap.remove(pieces);
			if (colorMap.isEmpty()) {
				this.pieceMap.remove(colorMap);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public King getKing(final Color color) {
		final PieceMap colorMap = this.pieceMap.get(color);
		if (colorMap == null) {
			throw new RuntimeException(color.toString() + " has no King");
		}
		final List<Piece> kings = colorMap.get(King.class);
		if (kings == null
				|| kings.size() == 0) {
			throw new RuntimeException(color.toString() + " has no King");
		}
		if (kings.size() > 1) {
			throw new RuntimeException(color.toString() + " has multiple Kings");
		}
		return (King)kings.get(0);
	}

	public boolean canColorMove(final Color color) {
		final PieceMap colorMap = this.pieceMap.get(color);
		if (colorMap == null) {
			return false;
		}
		return colorMap.values()
			.stream()
			.flatMap(List::stream)
			.anyMatch(Piece::canMove);
	}
}

