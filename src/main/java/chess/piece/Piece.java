package chess.piece;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import chess.Board;
import chess.Color;
import chess.Field;
import chess.move.EnPassant;
import chess.move.Move;
import chess.move.PawnSkip;
import chess.piece.movement.ConditionedMovement;
import chess.piece.movement.Movement;

import resource.ResourceManager;

public abstract class Piece extends Label {
	protected final ImageView imageView;
	protected final Color color;
	protected Field field;
	protected boolean hasMoved = false;
	protected boolean enPassantPossible = false;

	public Piece(final Color color, final Board board) {
		super();
		this.imageView = new ImageView(
				new Image(
						new File(
								ResourceManager.projectPath()
									+ File.separator
									+ "images"
									+ File.separator
									+ this.getClass().getSimpleName()
									+ "_"
									+ color
									+ ".png")
							.toURI()
							.toString()));
		this.imageView.fitWidthProperty().bind(board.sizeProperty().divide(10));
		this.imageView.fitHeightProperty().bind(board.sizeProperty().divide(10));
		this.setGraphic(this.imageView);
		this.color = color;
	}

	public Color getColor() {
		return this.color;
	}

	public Field getField() {
		return this.field;
	}

	public void setField(final Field field) {
		this.field = field;
	}

	public Move getMove(final int column, final int row) {
		return new Move(
				this,
				this.field,
				this.field.getRelativeField(column, row));
	}

	public Move getEnPassant(final int column, final int row) {
		return new EnPassant(this,
				this.field,
				this.field.getRelativeField(column, row));
	}

	public Move getPawnSkip(final int column, final int row) {
		return new PawnSkip(this,
				this.field,
				this.field.getRelativeField(column, row));
	}

	public void setMoved(final boolean hasMoved) {
		this.hasMoved = hasMoved;
	}

	public boolean hasMoved() {
		return this.hasMoved;
	}

	public void setEnPassantPossible(final boolean enPassantPossible) {
		this.enPassantPossible = enPassantPossible;
	}

	public boolean getEnPassantPossible() {
		return false;
	}

	protected boolean isFreeAt(
			final int column,
			final int row,
			final Piece piece) {
		if (!this.fieldExists(column, row, piece)) {
			return false;
		}
		return piece.getField().getRelativeField(column, row).getPiece() == null;
	}

	protected boolean canTakeAt(
			final int column,
			final int row,
			final Piece piece,
			final boolean enPassantRequired) {
		if (!this.fieldExists(column, row, piece)) {
			return false;
		}
		final Piece enemy = piece.getField().getRelativeField(column, row).getPiece();
		return enemy != null
				&& enemy.getColor() != piece.getColor()
				&& (enPassantRequired
						? enemy.getEnPassantPossible()
						: true);
	}

	protected boolean canMoveTo(
			final int column,
			final int row,
			final Piece piece) {
		if (!this.fieldExists(column, row, piece)) {
			return false;
		}
		final Piece enemy = piece.getField().getRelativeField(column,  row).getPiece();
		return enemy == null
				|| enemy.getColor() != piece.getColor();
	}

	protected boolean fieldExists(
			final int column,
			final int row,
			final Piece piece) {
		final int absoluteColumn = column + piece.getField().getColumn();
		final int absoluteRow = row + piece.getField().getRow();
		return absoluteRow >= 0
				&& absoluteColumn >= 0
				&& absoluteRow < Board.SIZE
				&& absoluteColumn < Board.SIZE;
	}

	public abstract List<ConditionedMovement> getMovingMovements();

	public abstract List<ConditionedMovement> getAttackingMovements();

	public List<Move> getMoves() {
		return Stream.concat(
				this.getMovingMovements().stream(),
				this.getAttackingMovements().stream())
			.flatMap(e -> e.getMovesFor(this).stream())
			.distinct()
			.filter(e -> !this.field.getBoard()
				.getKing(e.getPiece().getColor())
				.getField()
				.wouldBeAttackedByOtherThan(e, e.getColor()))
			.collect(Collectors.toList());
	};

	public boolean canMove() {
		return Stream.concat(
				this.getMovingMovements().stream(),
				this.getAttackingMovements().stream())
			.flatMap(e -> e.getMovesFor(this).stream())
			.anyMatch(e -> !this.field.getBoard()
				.getKing(e.getPiece().getColor())
				.getField()
				.wouldBeAttackedByOtherThan(e, e.getColor()));
	}

	@Override
	public String toString() {
		final String name = this.getClass().getSimpleName();
		return name.charAt(0) + name.substring(1).toLowerCase();
	}
}
