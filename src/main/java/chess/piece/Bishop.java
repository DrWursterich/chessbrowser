package chess.piece;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import chess.Board;
import chess.Color;
import chess.move.Move;
import chess.piece.movement.CapturingMovement;
import chess.piece.movement.ConditionedMovement;
import chess.piece.movement.Movement;

public class Bishop extends Piece {

	private final List<ConditionedMovement> movements = Arrays.asList(
			new CapturingMovement(Movement.UNLIMITED_DIAGONAL, this.color));

	public Bishop(final Color color, final Board board) {
		super(color, board);
	}

	@Override
	public List<Move> getMoves() {
		return this.movements.stream()
			.flatMap(e -> e.getMovesFor(this).stream())
			.filter(e -> !this.field.getBoard()
				.getKing(e.getPiece().getColor())
				.getField()
				.wouldBeAttackedByOtherThan(e, e.getColor()))
			.collect(Collectors.toList());
	}

	@Override
	public boolean canMove() {
		return this.movements.stream()
			.flatMap(e -> e.getMovesFor(this).stream())
			.anyMatch(e -> !this.field.getBoard()
				.getKing(e.getPiece().getColor())
				.getField()
				.wouldBeAttackedByOtherThan(e, e.getColor()));
	}

	@Override
	public List<ConditionedMovement> getMovingMovements() {
		return this.movements;
	}

	@Override
	public List<ConditionedMovement> getAttackingMovements() {
		return this.movements;
	}

	@Override
	public String toString() {
		return "B";
	}
}
