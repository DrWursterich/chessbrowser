package chess.piece;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import chess.Board;
import chess.Color;
import chess.Field;
import chess.move.EnPassant;
import chess.move.EnPassantPromote;
import chess.move.Move;
import chess.move.PawnSkip;
import chess.move.Promote;
import chess.piece.movement.ConditionedMovement;
import chess.piece.movement.Movement;
import chess.piece.movement.NonCapturingMovement;

public class Pawn extends Piece {

	private static final List<ConditionedMovement> MOVING_MOVEMENTS
			= Arrays.asList(
					new NonCapturingMovement(Movement.ONE_FORWARD) {
						@Override
						public List<Move> getMovesFor(final Piece piece) {
							final List<Move> moves = new ArrayList<>();
							if (this.movement.canBeUsedByPiece(piece)) {
								final int direction = piece.getColor()
										== Color.WHITE
									? Movement.DOWN
									: Movement.UP;
								this.movement.forEachFieldFrom(
										piece.getField(),
										direction,
										e -> {
											if (this.test(e)) {
												if (e.getPiece() != null) {
													return true;
												}
												if (e.getRelativeField(
														0,
														direction) == null) {
													final Board board
															= piece.getField()
																.getBoard();
													moves.add(
													 	 new Promote(
															piece,
															piece.getField(),
															e,
															new Bishop(
																piece.getColor(),
																board)));
													moves.add(
													 	 new Promote(
															piece,
															piece.getField(),
															e,
															new Knight(
																piece.getColor(),
																board)));
													moves.add(
													 	 new Promote(
															piece,
															piece.getField(),
															e,
															new Rook(
																piece.getColor(),
																board)));
													moves.add(
													 	 new Promote(
															piece,
															piece.getField(),
															e,
															new Queen(
																piece.getColor(),
																board)));
												} else {
													moves.add(
														new Move(
															piece,
															piece.getField(),
															e));
												}
											}
											return false;
										});
							}
							return moves;
						}
					},
					new NonCapturingMovement(Movement.TWO_FORWARD) {
						@Override
						public List<Move> getMovesFor(final Piece piece) {
							final List<Move> moves = new ArrayList<>();
							final int direction = piece.getColor()
									== Color.WHITE
								? Movement.DOWN
								: Movement.UP;
							if (this.movement.canBeUsedByPiece(piece)) {
								this.movement.forEachFieldFrom(
										piece.getField(),
										direction,
										e -> {
											if (this.test(e)) {
												if (e.getPiece() != null) {
													return true;
												}
												if (e.getRelativeField(
														0,
														direction) == null) {
													final Board board
															= piece.getField()
																.getBoard();
													moves.add(
													 	 new Promote(
															piece,
															piece.getField(),
															e,
															new Bishop(
																piece.getColor(),
																board)));
													moves.add(
													 	 new Promote(
															piece,
															piece.getField(),
															e,
															new Knight(
																piece.getColor(),
																board)));
													moves.add(
													 	 new Promote(
															piece,
															piece.getField(),
															e,
															new Rook(
																piece.getColor(),
																board)));
													moves.add(
													 	 new Promote(
															piece,
															piece.getField(),
															e,
															new Queen(
																piece.getColor(),
																board)));
												} else {
													moves.add(
														new PawnSkip(
																piece,
																piece.getField(),
																e));
												}
											}
											return false;
										});
							}
							return moves;
						}
					});

	private List<ConditionedMovement> attackingMovements = Arrays.asList(
			new ConditionedMovement(Movement.ONE_DIAGONALLY_FORWARD) {
				@Override
				public boolean test(final Field field) {
					if (field.getPiece() != null) {
						return field.getPiece().getColor() != Pawn.this.color;
					}
					final Field enPassantField = field.getRelativeField(
							0,
							Pawn.this.color == Color.WHITE
								? Movement.UP
								: Movement.DOWN);
					return enPassantField != null
							&& enPassantField.getPiece() instanceof Pawn
							&& enPassantField.getPiece().getColor()
									!= Pawn.this.color
							&& enPassantField.getPiece().getEnPassantPossible();
				}

				@Override
				public List<Move> getMovesFor(final Piece piece) {
					final List<Move> moves = new ArrayList<>();
					if (this.movement.canBeUsedByPiece(piece)) {
						final int direction = piece.getColor() == Color.WHITE
							? Movement.DOWN
							: Movement.UP;
						this.movement.forEachFieldFrom(
								piece.getField(),
								direction,
								e -> {
									if (this.test(e)) {
										final boolean isPromotion
											= e.getRelativeField(0, direction)
													== null;
										final boolean isEnPassant = e.getPiece()
												== null;
										if (isPromotion) {
											final Board board
												= piece.getField().getBoard();
											if (isEnPassant) {
												moves.add(
													new EnPassantPromote(
														piece,
														piece.getField(),
														e,
														new Bishop(
															piece.getColor(),
															board)));
												moves.add(
													new EnPassantPromote(
														piece,
														piece.getField(),
														e,
														new Knight(
															piece.getColor(),
															board)));
												moves.add(
													new EnPassantPromote(
														piece,
														piece.getField(),
														e,
														new Rook(
															piece.getColor(),
															board)));
												moves.add(
													new EnPassantPromote(
														piece,
														piece.getField(),
														e,
														new Queen(
															piece.getColor(),
															board)));
											} else {
												moves.add(
													new Promote(
														piece,
														piece.getField(),
														e,
														new Bishop(
															piece.getColor(),
															board)));
												moves.add(
													new Promote(
														piece,
														piece.getField(),
														e,
														new Knight(
															piece.getColor(),
															board)));
												moves.add(
													new Promote(
														piece,
														piece.getField(),
														e,
														new Rook(
															piece.getColor(),
															board)));
												moves.add(
													new Promote(
														piece,
														piece.getField(),
														e,
														new Queen(
															piece.getColor(),
															board)));
											}
										} else {
											moves.add(isEnPassant
												? new EnPassant(
														piece,
														piece.getField(),
														e)
												: new Move(
														piece,
														piece.getField(),
														e));
										}
									}
									return false;
								});
					}
					return moves;
				}
			});

	public Pawn(final Color color, final Board board) {
		super(color, board);
	}

	@Override
	public boolean getEnPassantPossible() {
		return this.enPassantPossible;
	}

	@Override
	public List<ConditionedMovement> getMovingMovements() {
		return Pawn.MOVING_MOVEMENTS;
	}

	@Override
	public List<ConditionedMovement> getAttackingMovements() {
		return this.attackingMovements;
	}

	@Override
	public String toString() {
		return "P";
	}
}
