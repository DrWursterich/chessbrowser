package chess.piece.movement;

import chess.Field;

public class NonCapturingMovement extends ConditionedMovement {

	public NonCapturingMovement(final Movement movement) {
		super(movement);
	}

	@Override
	public boolean test(final Field field) {
		return field.getPiece() == null;
	}
}

