package chess.piece.movement;

import chess.Color;
import chess.Field;
import chess.piece.King;

public class OppositionMovement extends CapturingMovement {

	private static final int[] coordinates = {1, 0, -1, 0, 1, -1, -1, 1};

	public OppositionMovement(final Movement movement, final Color color) {
		super(movement, color);
	}

	@Override
	public boolean test(final Field field) {
		return super.test(field)
				&& !this.isEnemyKingCloseTo(field);
	}

	private boolean isEnemyKingCloseTo(final Field field) {
		Field target = null;
		for (int i = coordinates.length - 1; i >= 0; i--) {
			target = field.getRelativeField(
					coordinates[i],
					coordinates[(i + 1) % coordinates.length]);
			if (field != null
					&& field.getPiece() instanceof King
					&& field.getPiece().getColor() != this.color) {
				return true;
			}
		}
		return false;
	}
}

