package chess.piece.movement;

import java.util.function.Predicate;

import chess.Color;
import chess.Field;
import chess.piece.Piece;

public enum Movement {
	UNLIMITED_DIAGONAL() {
		@Override
		public void forEachFieldFrom(
				final Field field,
				final int direction,
				final Predicate<Field> action) {
			Field target = null;
			for (int j = 3; j>= 0; j--) {
				for (int i = 1; true; i++) {
					target = field.getRelativeField(
							i * (j < 2 ? 1 : -1),
							i * ((j + 1) % 4 < 2 ? 1 : -1));
					if (target == null
							|| action.test(target)) {
						break;
					}
				}
			}
		}

		@Override
		public void forEachAttackingPiece(
				final Field field,
				final Piece pieceToIgnore,
				final Predicate<Piece> action) {
			Field target = null;
			Piece attacker = null;
			for (int j = 3; j>= 0; j--) {
				for (int i = 1; true; i++) {
					target = field.getRelativeField(
							i * (j < 2 ? 1 : -1),
							i * ((j + 1) % 4 < 2 ? 1 : -1));
					if (target == null) {
						break;
					}
					attacker = target.getPiece();
					if (attacker == null) {
						continue;
					}
					if (attacker == pieceToIgnore) {
						continue;
					}
					for (final ConditionedMovement movement
							: attacker.getAttackingMovements()) {
						if (movement.getMovement() == this
								&& movement.test(field)) {
							if (action.test(attacker)) {
								return;
							}
							break;
						}
					}
					break;
				}
			}
		}
	},
	UNLIMITED_STRAIGHT() {
		private final int[] coordinates = {1, 0, -1, 0};

		@Override
		public void forEachFieldFrom(
				final Field field,
				final int direction,
				final Predicate<Field> action) {
			Field target = null;
			for (int j = coordinates.length - 1; j >= 0; j--) {
				for (int i = 1; true; i++) {
					target = field.getRelativeField(
							coordinates[j] * i,
							coordinates[(j + 1) % coordinates.length] * i);
					if (target == null
							|| action.test(target)) {
						break;
					}
				}
			}
		}

		@Override
		public void forEachAttackingPiece(
				final Field field,
				final Piece pieceToIgnore,
				final Predicate<Piece> action) {
			Field target = null;
			Piece attacker = null;
			for (int j = coordinates.length - 1; j >= 0; j--) {
				for (int i = 1; true; i++) {
					target = field.getRelativeField(
							coordinates[j] * i,
							coordinates[(j + 1) % coordinates.length] * i);
					if (target == null) {
						break;
					}
					attacker = target.getPiece();
					if (attacker == null) {
						continue;
					}
					if (attacker == pieceToIgnore) {
						continue;
					}
					for (final ConditionedMovement movement
							: attacker.getAttackingMovements()) {
						if (movement.getMovement() == this
								&& movement.test(field)) {
							if (action.test(attacker)) {
								return;
							}
							break;
						}
					}
					break;
				}
			}
		}
	},
	ONE_DIAGONAL() {
		@Override
		public void forEachFieldFrom(
				final Field field,
				final int direction,
				final Predicate<Field> action) {
			Field target = null;
			for (int i = 3; i>= 0; i--) {
				target = field.getRelativeField(
						i < 2 ? 1 : -1,
						(i + 1) % 4 < 2 ? 1 : -1);
				if (target != null) {
					action.test(target);
				}
			}
		}

		@Override
		public void forEachAttackingPiece(
				final Field field,
				final Piece pieceToIgnore,
				final Predicate<Piece> action) {
			Field target = null;
			Piece attacker = null;
			for (int i = 3; i>= 0; i--) {
				target = field.getRelativeField(
						i < 2 ? 1 : -1,
						(i + 1) % 4 < 2 ? 1 : -1);
				if (target == null) {
					continue;
				}
				attacker = target.getPiece();
				if (attacker == null) {
					continue;
				}
				if (attacker == pieceToIgnore) {
					continue;
				}
				for (final ConditionedMovement movement
						: attacker.getAttackingMovements()) {
					if (movement.getMovement() == this
							&& movement.test(field)) {
						if (action.test(attacker)) {
							return;
						}
						break;
					}
				}
			}
		}
	},
	ONE_STRAIGHT() {
		final int[] coordinates = {1, 0, -1, 0};
		@Override
		public void forEachFieldFrom(
				final Field field,
				final int direction,
				final Predicate<Field> action) {
			Field target = null;
			for (int i = coordinates.length - 1; i >= 0; i--) {
				target = field.getRelativeField(
						coordinates[i],
						coordinates[(i + 1) % coordinates.length]);
				if (target != null) {
					action.test(target);
				}
			}
		}

		@Override
		public void forEachAttackingPiece(
				final Field field,
				final Piece pieceToIgnore,
				final Predicate<Piece> action) {
			Field target = null;
			Piece attacker = null;
			for (int i = coordinates.length - 1; i >= 0; i--) {
				target = field.getRelativeField(
						coordinates[i],
						coordinates[(i + 1) % coordinates.length]);
				if (target == null) {
					continue;
				}
				attacker = target.getPiece();
				if (attacker == null) {
					continue;
				}
				if (attacker == pieceToIgnore) {
					continue;
				}
				for (final ConditionedMovement movement
						: attacker.getAttackingMovements()) {
					if (movement.getMovement() == this
							&& movement.test(field)) {
						if (action.test(attacker)) {
							return;
						}
						break;
					}
				}
			}
		}
	},
	ONE_FORWARD() {
		@Override
		public void forEachFieldFrom(
				final Field field,
				final int direction,
				final Predicate<Field> action) {
			final Field target = field.getRelativeField(0, direction);
			if (target != null) {
				action.test(target);
			}
		}

		@Override
		public void forEachAttackingPiece(
				final Field field,
				final Piece pieceToIgnore,
				final Predicate<Piece> action) {
			/*Field target = null;
			Piece attacker = null;
			for (int i = 1; i >= -1; i -= 1) {
				target = field.getRelativeField(
						coordinates[i],
						coordinates[(i + 1) % coordinates.length]);
				if (target == null) {
					continue;
				}
				attacker = target.getPiece();
				if (attacker == null) {
					continue;
				}
				if (attacker == pieceToIgnore) {
					continue;
				}
				if (attacker.getColor() == Color.WHITE ^ i == 1) {
					continue;
				}
				for (final ConditionedMovement movement
						: attacker.getAttackingMovements()) {
					if (movement.getMovement() == this
							&& movement.test(field)) {
						if (action.test(attacker)) {
							return;
						}
						break;
					}
				}
			}*/
		}
	},
	TWO_FORWARD() {
		@Override
		public void forEachFieldFrom(
				final Field field,
				final int direction,
				final Predicate<Field> action) {
			final Field first = field.getRelativeField(0, direction);
			if (first == null || first.getPiece() != null) {
				return;
			}
			final Field target = field.getRelativeField(0, direction * 2);
			if (target != null) {
				action.test(target);
			}
		}

		@Override
		public void forEachAttackingPiece(
				final Field field,
				final Piece pieceToIgnore,
				final Predicate<Piece> action) {
			/*Field first = null;
			Field target = null;
			Piece attacker = null;
			for (int i = 1; i >= -1; i -= 2) {
				first = field.getRelativeField(0, i);
				if (first == null || first.getPiece() != null) {
					continue;
				}
				target = field.getRelativeField(0, i * 2);
				if (target == null) {
					continue;
				}
				attacker = target.getPiece();
				if (attacker == null) {
					continue;
				}
				if (attacker == pieceToIgnore) {
					continue;
				}
				if (attacker.getColor() == Color.WHITE ^ i == 1) {
					continue;
				}
				for (final ConditionedMovement movement
						: attacker.getAttackingMovements()) {
					if (movement.getMovement() == this
							&& movement.test(field)) {
						if (action.test(attacker)) {
							return;
						}
						break;
					}
				}
			}*/
		}

		@Override
		public boolean canBeUsedByPiece(final Piece piece) {
			return !piece.hasMoved();
		}
	},
	ONE_DIAGONALLY_FORWARD() {
		@Override
		public void forEachFieldFrom(
				final Field field,
				final int direction,
				final Predicate<Field> action) {
			Field target = null;
			for (int i = 1; i >= -1; i -= 2) {
				target = field.getRelativeField(i, direction);
				if (target != null) {
					action.test(target);
				}
			}
		}

		@Override
		public void forEachAttackingPiece(
				final Field field,
				final Piece pieceToIgnore,
				final Predicate<Piece> action) {
			Field target = null;
			Piece attacker = null;
			for (int i = 1; i >= -1; i -= 2) {
				target = field.getRelativeField(i, i);
				if (target == null) {
					continue;
				}
				attacker = target.getPiece();
				if (attacker == null) {
					continue;
				}
				if (attacker == pieceToIgnore) {
					continue;
				}
				if (attacker.getColor() == Color.WHITE ^ i == 1) {
					continue;
				}
				for (final ConditionedMovement movement
						: attacker.getAttackingMovements()) {
					if (movement.getMovement() == this
							&& movement.test(field)) {
						if (action.test(attacker)) {
							return;
						}
						break;
					}
				}
			}
		}
	},
	L_JUMP() {
		private final int[] coordinates = {2, 1, 2, -1, -2, 1, -2, -1};

		@Override
		public void forEachFieldFrom(
				final Field field,
				final int direction,
				final Predicate<Field> action) {
			Field target = null;
			for (int i = coordinates.length - 1; i >= 0; i--) {
				target = field.getRelativeField(
						coordinates[i],
						coordinates[(i + 1) % coordinates.length]);
				if (target != null) {
					action.test(target);
				}
			}
		}

		@Override
		public void forEachAttackingPiece(
				final Field field,
				final Piece pieceToIgnore,
				final Predicate<Piece> action) {
			Field target = null;
			Piece attacker = null;
			for (int i = coordinates.length - 1; i >= 0; i--) {
				target = field.getRelativeField(
						coordinates[i],
						coordinates[(i + 1) % coordinates.length]);
				if (target == null) {
					continue;
				}
				attacker = target.getPiece();
				if (attacker == null) {
					continue;
				}
				if (attacker == pieceToIgnore) {
					continue;
				}
				for (final ConditionedMovement movement
						: attacker.getAttackingMovements()) {
					if (movement.getMovement() == this
							&& movement.test(field)) {
						if (action.test(attacker)) {
							return;
						}
						break;
					}
				}
			}
		}
	},
	CASTLE() {
		@Override
		public void forEachFieldFrom(
				final Field field,
				final int direction,
				final Predicate<Field> action) {
			Field target = null;
			for (int i = 2; i >= -2; i -= 4) {
				target = field.getRelativeField(i, 0);
				if (target != null) {
					action.test(target);
				}
			}
		}

		@Override
		public boolean canBeUsedByPiece(final Piece piece) {
			return !piece.hasMoved();
		}

		@Override
		public void forEachAttackingPiece(
				final Field field,
				final Piece pieceToIgnore,
				final Predicate<Piece> action) {
		}
	};

	public static final int UP = 1;
	public static final int DOWN = -1;
	public static final int ANY_DIRECTION = 0;

	public abstract void forEachFieldFrom(
			final Field field,
			final int direction,
			final Predicate<Field> action);

	public abstract void forEachAttackingPiece(
			final Field field,
			final Piece pieceToIgnore,
			final Predicate<Piece> action);

	public boolean canBeUsedByPiece(final Piece piece) {
		return true;
	}
}

