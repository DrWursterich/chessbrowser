package chess.piece.movement;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import chess.Color;
import chess.Field;
import chess.move.Move;
import chess.piece.Piece;

public abstract class ConditionedMovement implements Predicate<Field> {
	protected final Movement movement;

	public ConditionedMovement(final Movement movement) {
		this.movement = movement;
	}

	public Movement getMovement() {
		return this.movement;
	}

	public List<Move> getMovesFor(final Piece piece) {
		final List<Move> moves = new ArrayList<>();
		if (this.movement.canBeUsedByPiece(piece)) {
			this.movement.forEachFieldFrom(
					piece.getField(),
					piece.getColor() == Color.WHITE
						? Movement.DOWN
						: Movement.UP,
					e -> {
						if (this.test(e)) {
							if (e.getPiece() != null) {
								return true;
							}
							moves.add(new Move(piece, piece.getField(), e));
						}
						return false;
					});
		}
		return moves;
	}
}

