package chess.piece.movement;

import java.util.ArrayList;
import java.util.List;

import chess.Color;
import chess.Field;
import chess.move.Move;
import chess.piece.Piece;

public class CapturingMovement extends ConditionedMovement {
	protected final Color color;

	public CapturingMovement(final Movement movement, final Color color) {
		super(movement);
		this.color = color;
	}

	@Override
	public boolean test(final Field field) {
		final Piece attackedPiece = field.getPiece();
		return attackedPiece == null
				|| attackedPiece.getColor() != this.color;
	}

	@Override
	public List<Move> getMovesFor(final Piece piece) {
		final List<Move> moves = new ArrayList<>();
		if (this.movement.canBeUsedByPiece(piece)) {
			this.movement.forEachFieldFrom(
					piece.getField(),
					piece.getColor() == Color.WHITE
						? Movement.UP
						: Movement.DOWN,
					e -> {
						if (this.test(e)) {
							if (e.getPiece() == null) {
								moves.add(new Move(piece, piece.getField(), e));
								return false;
							} else if (e.getPiece().getColor() != this.color) {
								moves.add(new Move(piece, piece.getField(), e));
							}
						}
						return true;
					});
		}
		return moves;
	}
}

