package chess.piece;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import chess.Board;
import chess.Color;
import chess.Field;
import chess.move.Castle;
import chess.move.Move;
import chess.piece.movement.OppositionMovement;
import chess.piece.movement.ConditionedMovement;
import chess.piece.movement.Movement;

public class King extends Piece {

	public final List<ConditionedMovement> movingMovements = Arrays.asList(
			new ConditionedMovement(Movement.CASTLE) {
				@Override
				public boolean test(final Field field) {
					return King.this.canCastleTowards(
							Integer.signum(
									field.getColumn()
										- King.this.field.getColumn()));
				}

				@Override
				public List<Move> getMovesFor(final Piece piece) {
					final List<Move> moves = new ArrayList<>();
					if (this.movement.canBeUsedByPiece(piece)) {
						this.movement.forEachFieldFrom(
								piece.getField(),
								piece.getColor() == Color.WHITE
									? Movement.UP
									: Movement.DOWN,
								e -> {
									if (this.test(e)) {
										moves.add(
												new Castle(
														piece,
														piece.getField(),
														e));
									}
									return false;
								});
					}
					return moves;
				}
			});

	private final List<ConditionedMovement> attackingMovements = Arrays.asList(
			new OppositionMovement(Movement.ONE_STRAIGHT, this.color),
			new OppositionMovement(Movement.ONE_DIAGONAL, this.color));

	public King(final Color color, final Board board) {
		super(color, board);
	}

	@Override
	public List<Move> getMoves() {
		return Stream.concat(
				this.getMovingMovements().stream(),
				this.getAttackingMovements().stream())
			.flatMap(e -> e.getMovesFor(this).stream())
			.filter(e -> !e.getTo().wouldBeAttackedByOtherThan(e, this.color))
			.collect(Collectors.toList());
	}

	@Override
	public boolean canMove() {
		return Stream.concat(
				this.getMovingMovements().stream(),
				this.getAttackingMovements().stream())
			.flatMap(e -> e.getMovesFor(this).stream())
			.anyMatch(e -> !e.getTo().wouldBeAttackedByOtherThan(e, this.color));
	}

	private boolean canCastleTowards(final int direction) {
		// also accounts for unusual starting positions of the rook
		int i = 3;
		boolean foundUnmovedRook = false;
		Field field = this.field;
		do {
			// do not abort when the field the king would land on
			// is under attack since in unusual starting positions could be
			// an attacking piece behind the rook, which isAttackedBy cannot
			// spot but wouldBeAttackedBy will - which we need the move for
			if (i-- > 1 && field.isAttackedByOtherThan(this.color, this)) {
				return false;
			}
			if (field.getPiece() == null
					|| i == 2) {
				if (foundUnmovedRook) {
					return true;
				}
				continue;
			}
			if (!(field.getPiece() instanceof Rook)) {
				return false;
			}
			if (field.getPiece().hasMoved()) {
				return false;
			}
			if (field.getPiece().getColor() != this.color) {
				return false;
			}
			// when an unmoved rook is the direct neighbour of the king
			// we have to check one field further where the king would land
			if (i == 1) {
				foundUnmovedRook = true;
			} else {
				return true;
			}
		} while ((field = field.getRelativeField(direction, 0)) != null);
		return false;
	}

	@Override
	public List<ConditionedMovement> getMovingMovements() {
		return this.movingMovements;
	}

	@Override
	public List<ConditionedMovement> getAttackingMovements() {
		return this.attackingMovements;
	}

	@Override
	public String toString() {
		return "K";
	}
}
