package chess;

public enum Color {
	WHITE,
	BLACK;

	private boolean isInCheck = false;

	@Override
	public String toString() {
		return this.name().charAt(0) + this.name().substring(1).toLowerCase();
	}

	public void setInCheck(final boolean value) {
		this.isInCheck = value;
	}

	public boolean isInCheck() {
		return this.isInCheck;
	}
}

