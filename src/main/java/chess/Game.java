package chess;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java.io.IOException;

import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;

import chess.move.Move;
import chess.piece.King;

public class Game extends StackPane {
	private final Board board;
	private final List<Turn> turns;
	private final Player[] players;
	private List<Move> moves;
	private int playerToMove;

	public Game() throws IOException {
		this.board = new Board(this);
		this.turns = new ArrayList<>();
		this.players = new Player[] {
			new Player(Color.WHITE),
			new Player(Color.BLACK)
		};
		this.playerToMove = 0;
		HBox.setHgrow(this.board, Priority.ALWAYS);
		this.getChildren().add(this.board);
	}

	public List<Player> getPlayers() {
		return Arrays.asList(this.players);
	}

	public void setup() {
		this.turns.clear();
		this.turns.add(new Turn());
		this.playerToMove = 0;
		this.board.setup();
	}

	public Turn getPreviousTurn() {
		return this.turns.size() > 1
				? this.turns.get(this.turns.size() - 2)
				: null;
	}

	public Turn getCurrentTurn() {
		return this.turns.size() > 0
				? this.turns.get(this.turns.size() - 1)
				: null;
	}

	public Move getLastMoveOf(final Color color) {
		for (int i = this.turns.size() - 1; i >= 0; i--) {
			final Turn turn = this.turns.get(i);
			if (turn != null) {
				final Move move = turn.getLastMoveOf(color);
				if (move != null) {
					return move;
				}
			}
		}
		return null;
	}

	public int getMoveNumber() {
		final int movesPerTurn = this.players.length;
		return 1 + (this.turns.size() - 1) * movesPerTurn
				+ this.getCurrentTurn().getMoves().size();
	}

	public void clickedOnField(final Field field) {
		if (this.moves != null) {
			final List<Move> movesForClickedField = new ArrayList<>(4);
			for (final Move move : this.moves) {
				move.getFieldToHighlight().setHighlited(false);
				if (move.getFieldToHighlight().equals(field)) {
					movesForClickedField.add(move);
				}
			}
			this.moves = null;
			this.executeMoves(movesForClickedField);
		} else if (field.getPiece() != null
				&& field.getPiece().getColor()
						== this.players[this.playerToMove].getColor()) {
			this.moves = field.getPiece().getMoves();
			for (final Move move : this.moves) {
				move.getFieldToHighlight().setHighlited(true);
			}
		}
	}

	private void executeMoves(final List<Move> moves) {
		final Move move;
		switch (moves.size()) {
			case 0:
				return;
			case 1:
				move = moves.get(0);
				break;
			default:
				move = this.letPlayerChooseMove(moves);
		}
		if (move == null) {
			return;
		}
		move.execute();
		this.nextPlayersTurn();
		this.getCurrentTurn().addMove(move);
		final Player playerToMove = this.players[this.playerToMove];
		final boolean isCheck = this.board.getKing(playerToMove.getColor())
			.getField()
			.isAttackedByOtherThan(playerToMove.getColor(), null);
		playerToMove.getColor().setInCheck(isCheck);
		final boolean canMove = this.board.canColorMove(
				playerToMove.getColor());
		System.out.printf(
				"%3d. %10s%s\n",
				this.getMoveNumber(),
				move.toString(),
				isCheck ? canMove ? "+" : "#" : "");
		if (!canMove) {
			System.out.println(
					playerToMove.getColor().toString()
						+ (isCheck
							? " won by checkmate!"
							: " has no possible moves left - stalemate!"));
		}
	}

	private Move letPlayerChooseMove(final List<Move> moves) {
		final ChoiceDialog<Move> dialog = new ChoiceDialog<>();
		dialog.getItems().addAll(moves);
		dialog.setTitle("Choose a Move");
		final Move choice = dialog.showAndWait().orElse(null);
		dialog.close();
		return choice;
	}

	private void nextPlayersTurn() {
		this.playerToMove = (this.playerToMove + 1) % this.players.length;
		if (this.playerToMove - 1 == this.players.length) {
			this.turns.add(new Turn());
		}
		final Move lastMove = this.getLastMoveOf(
				this.players[this.playerToMove].getColor());
		if (lastMove != null) {
			lastMove.beforeNextTurn();
		}
	}
}
