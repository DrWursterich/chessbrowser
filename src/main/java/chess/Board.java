package chess;

import static chess.Color.BLACK;
import static chess.Color.WHITE;

import java.io.IOException;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.NumberBinding;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

import resource.ResourceManager;

import chess.move.Move;
import chess.piece.Bishop;
import chess.piece.King;
import chess.piece.Knight;
import chess.piece.Pawn;
import chess.piece.Piece;
import chess.piece.PieceInhabitor;
import chess.piece.Queen;
import chess.piece.Rook;

/**
 * @author Mario Schäper
 */
public class Board extends StackPane {

	public static final int SIZE = 8;

	private final Game game;
	private final PieceInhabitor inhabitor;
	private final Field[][] fields;
	private final NumberBinding sizeProperty = Bindings.min(
			this.widthProperty(), this.heightProperty());

	public NumberBinding sizeProperty() {
		return this.sizeProperty;
	}

	public Board(final Game game) throws IOException {
		super();
		this.game = game;
		this.inhabitor = new PieceInhabitor();
		final GridPane gridpane = (GridPane)ResourceManager.getResource(
				"chess.Board");
		gridpane.maxWidthProperty().bind(this.sizeProperty());
		gridpane.maxHeightProperty().bind(this.sizeProperty());
		this.fields = new Field[8][];
		for (int i = SIZE - 1; i >= 0; i--) {
			this.fields[i] = new Field[8];
			for (int j = SIZE - 1; j >= 0; j--) {
				final Field field = new Field(this, i, j);
				this.fields[i][j] = field;
				field.getStyleClass().add(
						(i + j) % 2 == 0 ? "white" : "black");
				gridpane.add(field, i, j);
			}
		}
		this.getChildren().add(gridpane);
	}

	public void setup() {
		this.addPieceAt(new Rook(BLACK, this), 0, 0);
		this.addPieceAt(new Knight(BLACK, this), 1, 0);
		this.addPieceAt(new Bishop(BLACK, this), 2, 0);
		this.addPieceAt(new Queen(BLACK, this), 3, 0);
		this.addPieceAt(new King(BLACK, this), 4, 0);
		this.addPieceAt(new Bishop(BLACK, this), 5, 0);
		this.addPieceAt(new Knight(BLACK, this), 6, 0);
		this.addPieceAt(new Rook(BLACK, this), 7, 0);

		this.addPieceAt(new Rook(WHITE, this), 0, SIZE - 1);
		this.addPieceAt(new Knight(WHITE, this), 1, SIZE - 1);
		this.addPieceAt(new Bishop(WHITE, this), 2, SIZE - 1);
		this.addPieceAt(new Queen(WHITE, this), 3, SIZE - 1);
		this.addPieceAt(new King(WHITE, this), 4, SIZE - 1);
		this.addPieceAt(new Bishop(WHITE, this), 5, SIZE - 1);
		this.addPieceAt(new Knight(WHITE, this), 6, SIZE - 1);
		this.addPieceAt(new Rook(WHITE, this), 7, SIZE - 1);

		for (int i = SIZE - 1; i >= 0; i--) {
			this.addPieceAt(new Pawn(BLACK, this), i, 1);
			this.addPieceAt(new Pawn(WHITE, this), i, SIZE - 2);
		}
	}

	public boolean fieldExists(final int column, final int row) {
		return column >= 0
				&& column < Board.SIZE
				&& row >= 0
				&& row < Board.SIZE;
	}

	public Field getField(final int column, final int row) {
		return this.fields[column][row];
	}

	public void clickedOnField(final Field field) {
		this.game.clickedOnField(field);
	}

	public boolean canColorMove(final Color color) {
		return this.inhabitor.canColorMove(color);
	}

	public King getKing(final Color color) {
		return this.inhabitor.getKing(color);
	}

	public void removePiece(final Piece piece) {
		piece.getField().setPiece(null);
		piece.setField(null);
		this.inhabitor.removePiece(piece);
	}

	public void addPieceAt(final Piece piece, final int colum, final int row) {
		final Field field = this.fields[colum][row];
		field.setPiece(piece);
		piece.setField(field);
		this.inhabitor.addPiece(piece);
	}
}

