package chess.move;

import chess.Field;
import chess.piece.Piece;

public class PawnSkip extends Move {

	public PawnSkip(final Piece piece, final Field from, final Field to) {
		super(piece, from, to);
	}

	@Override
	public void execute() {
		super.execute();
		this.getPiece().setEnPassantPossible(true);
	}

	@Override
	public void undo() {
		super.undo();
		this.getPiece().setEnPassantPossible(false);
	}

	@Override
	public void beforeNextTurn() {
		this.getPiece().setEnPassantPossible(false);
	}

	@Override
	public void undoBeforeNextTurn() {
		this.getPiece().setEnPassantPossible(true);
	}
}
