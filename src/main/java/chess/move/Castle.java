package chess.move;

import chess.Field;
import chess.piece.Rook;
import chess.piece.Piece;

public class Castle extends Move {
	private final Field rookPosition;
	private final Field rookTarget;

	public Castle(final Piece piece, final Field from, final Field to) {
		super(piece, from, to);
		final int direction = (int)Math.signum(
				this.getTo().getColumn() - this.getFrom().getColumn());
		this.rookPosition = findRook(direction);
		rookTarget = this.getFrom().getRelativeField(direction,  0);
	}

	@Override
	public Field getFieldToHighlight() {
		return this.rookPosition;
	}

	@Override
	public boolean isCapturing() {
		return false;
	}

	@Override
	public void execute() {
		this.getTo().setPiece(this.getPiece());
		this.rookTarget.setPiece(this.rookPosition.getPiece());
		this.getPiece().setField(this.getTo());
		this.rookTarget.getPiece().setField(this.rookTarget);
		this.rookPosition.setPiece(null);
		this.getFrom().setPiece(null);
		this.getPiece().setMoved(true);
		this.rookTarget.getPiece().setMoved(true);
	}

	@Override
	public void undo() {
		this.getFrom().setPiece(this.getPiece());
		this.getPiece().setField(this.getFrom());
		this.getTo().setPiece(null);
		this.rookPosition.setPiece(this.rookTarget.getPiece());
		this.rookTarget.getPiece().setField(this.rookPosition);
		this.rookTarget.setPiece(null);
		this.getPiece().setMoved(false);
		this.rookPosition.getPiece().setMoved(false);
	}

	@Override
	public String toString() {
		return this.getTo().getColumn() < this.getFrom().getColumn()
			? "O-O-O"
			: "O-O";
	}

	private Field findRook(final int direction) {
		Field field = this.getFrom();
		while ((field = field.getRelativeField(direction, 0)) != null) {
			if (field.getPiece() instanceof Rook) {
				return field;
			}
		}
		throw new RuntimeException(
				"cannot create castling-move since there is no rook "
					+ (direction < 0 ? "left" : "right") + " from the "
					+ this.getPiece().getClass().getSimpleName()
					+ " on " + this.getFrom().toString());
	}
}
