package chess.move;

import chess.Field;
import chess.piece.Piece;

public class EnPassantPromote extends Move {
	private final Piece promotedTo;
	private Piece captured = null;

	public EnPassantPromote(
			final Piece piece,
			final Field from,
			final Field to,
			final Piece promotedTo) {
		super(piece, from, to);
		this.promotedTo = promotedTo;
	}

	@Override
	public boolean isCapturing() {
		return true;
	}

	@Override
	public void execute() {
		this.captured = this.getAttackedField().capturePiece();
		this.getFrom().capturePiece();
		this.getTo().addPiece(this.promotedTo);
		this.promotedTo.setMoved(true);
	}

	@Override
	public void undo() {
		this.getTo().capturePiece();
		this.getFrom().addPiece(this.getPiece());
		if (this.captured != null) {
			this.getAttackedField().addPiece(this.captured);
		}
	}

	private Field getAttackedField() {
		return this.getFrom().getRelativeField(
				this.getTo().getColumn() - this.getFrom().getColumn(),
				0);
	}

	@Override
	public String toString() {
		return super.toString() + "e.p.=" + this.promotedTo.toString();
	}
}
