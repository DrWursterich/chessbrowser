package chess.move;

import chess.Color;
import chess.Field;
import chess.piece.Pawn;
import chess.piece.Piece;

public class Move {
	private final Piece piece;
	private final Field from;
	private final Field to;
	private final boolean isCapturing;

	private Piece captured = null;
	private boolean pieceHadMovedBefore = false;

	public Move(final Piece piece, final Field from, final Field to) {
		this.piece = piece;
		this.from = from;
		this.to = to;
		this.isCapturing = to.getPiece() != null
				&& to.getPiece().getColor() != piece.getColor();
	}

	public Piece getPiece() {
		return this.piece;
	}

	public Field getFrom() {
		return this.from;
	}

	public Field getTo() {
		return this.to;
	}

	public boolean isCapturing() {
		return this.isCapturing;
	}

	public Field getFieldToHighlight() {
		return this.getTo();
	}

	public Color getColor() {
		return this.piece.getColor();
	}

	public void execute() {
		this.captured = this.to.capturePiece();
		this.pieceHadMovedBefore = this.piece.hasMoved();
		this.to.setPiece(this.piece);
		this.from.setPiece(null);
		this.piece.setField(this.to);
		this.piece.setMoved(true);
	}

	public void undo() {
		this.from.setPiece(this.piece);
		if (this.captured != null) {
			this.to.addPiece(this.captured);
		} else {
			this.to.setPiece(null);
		}
		this.piece.setField(this.from);
		this.piece.setMoved(this.pieceHadMovedBefore);
	}

	public void beforeNextTurn() {}

	public void undoBeforeNextTurn() {}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Move)) {
			return false;
		}
		final Move that = (Move)other;
		return this.from == that.from
				&& this.to == that.to
				&& this.piece == that.piece;
	}

	@Override
	public String toString() {
		return this.piece.toString()
			+ (this.isCapturing ? "x" : "")
			+ this.to.toString();
	}
}

