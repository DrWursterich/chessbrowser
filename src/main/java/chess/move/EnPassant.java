package chess.move;

import chess.Field;
import chess.piece.Piece;

public class EnPassant extends Move {
	private Piece captured = null;

	public EnPassant(final Piece piece, final Field from, final Field to) {
		super(piece, from, to);
	}

	@Override
	public boolean isCapturing() {
		return true;
	}

	@Override
	public void execute() {
		super.execute();
		this.captured = this.getAttackedField().capturePiece();
	}

	@Override
	public void undo() {
		super.undo();
		if (this.captured != null) {
			this.getAttackedField().addPiece(this.captured);
		}
	}

	private Field getAttackedField() {
		return this.getFrom().getRelativeField(
				this.getTo().getColumn() - this.getFrom().getColumn(),
				0);
	}

	@Override
	public String toString() {
		return super.toString() + "e.p.";
	}
}
