package chess.move;

import chess.Field;
import chess.piece.Piece;

public class Promote extends Move {
	private final Piece promotedTo;
	private Piece captured;

	public Promote(
			final Piece piece,
			final Field from,
			final Field to,
			final Piece promotedTo) {
		super(piece, from, to);
		this.promotedTo = promotedTo;
	}

	@Override
	public void execute() {
		this.captured = this.getTo().capturePiece();
		this.getFrom().capturePiece();
		this.getTo().addPiece(this.promotedTo);
		this.promotedTo.setMoved(true);
	}

	@Override
	public void undo() {
		this.getTo().capturePiece();
		if (this.captured != null) {
			this.getTo().addPiece(this.captured);
		}
		this.getFrom().addPiece(this.getPiece());
	}

	@Override
	public String toString() {
		return super.toString() + "=" + this.promotedTo.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Promote)) {
			return false;
		}
		final Promote that = (Promote)other;
		return super.equals(that)
				&& this.promotedTo.getClass().equals(
						that.promotedTo.getClass());
	}
}
