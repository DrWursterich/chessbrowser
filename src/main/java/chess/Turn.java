package chess;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import chess.move.Move;

public class Turn {
	private final List<Move> moves;

	public Turn(final Move... moves) {
		this.moves = new ArrayList<>(Arrays.asList(moves));
	}

	public Move getLastMoveOf(final Color color) {
		for (int i = this.moves.size() - 1; i >= 0; i--) {
			final Move move = this.moves.get(i);
			if (move.getColor() == color) {
				return move;
			}
		}
		return null;
	}

	public void addMove(final Move move) {
		this.moves.add(move);
	}

	public List<Move> getMoves() {
		return Collections.unmodifiableList(this.moves);
	}
}
